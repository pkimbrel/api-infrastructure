module "api" {
  source = "./modules/terraform-aws-api"

  suite             = "api"
  description       = "Sample API with multiple endpoints"
  domain            = local.domain
  subdomain         = local.app_domain
  ou                = local.ou
  environment       = local.environment
  account           = local.account
  allow_methods     = ["GET"]
  allow_origins     = ["http://locahost:3000"]
  allow_credentials = true

  lambdas = {
    todos-get = {
      description = "Todos get"
      handler     = "index.handler"
      runtime     = "nodejs20.x"
      policies    = { "dynamo_policy" = data.aws_iam_policy_document.dynamo_policy.json }
    },
    todo-put = {
      description = "Todo put"
      handler     = "index.handler"
      runtime     = "nodejs20.x"
      policies    = { "dynamo_policy" = data.aws_iam_policy_document.dynamo_policy.json }
    },
    todo-post = {
      description = "Todo post"
      handler     = "index.handler"
      runtime     = "nodejs20.x"
      policies    = { "dynamo_policy" = data.aws_iam_policy_document.dynamo_policy.json }
    },
    todo-delete = {
      description = "Todo delete"
      handler     = "index.handler"
      runtime     = "nodejs20.x"
      policies    = { "dynamo_policy" = data.aws_iam_policy_document.dynamo_policy.json }
    }
  }

  routes = {
    "todos_get" = {
      route                = "/todos"
      lambda               = "todos-get"
      method               = "GET"
      authorization_type   = "NONE"
      authorization_scopes = []
    },
    "todo_put" = {
      route                = "/todos/{id}"
      lambda               = "todo-put"
      method               = "PUT"
      authorization_type   = "NONE"
      authorization_scopes = []
    }
    "todo_put" = {
      route                = "/todos"
      lambda               = "todo-post"
      method               = "POST"
      authorization_type   = "NONE"
      authorization_scopes = []
    }
    "todos_delete" = {
      route                = "/todos/{id}"
      lambda               = "todo-delete"
      method               = "DELETE"
      authorization_type   = "NONE"
      authorization_scopes = []
    }
  }
}

data "aws_iam_policy_document" "dynamo_policy" {
  statement {
    actions   = ["dynamodb:*"]
    resources = [aws_dynamodb_table.api_state.arn]
    effect    = "Allow"
  }
}

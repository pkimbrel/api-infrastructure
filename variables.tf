variable "AWS_ACCESS_KEY_ID" {
  description = "AWS access key for deployment"
  type        = string
  default     = null
  sensitive   = true
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS secret key for deployment"
  type        = string
  default     = null
  sensitive   = true
}

data "aws_caller_identity" "current" {}

data "aws_vpc" "vpc_region1" {
  default = true
}

data "aws_subnets" "subnets_region1" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc_region1.id]
  }
}

data "aws_s3_bucket" "my_bucket" {
  bucket = "com.paulkimbrel.dev.us-east-1.deployment"
}

output "s3_bucket_details" { value = data.aws_s3_bucket.my_bucket }

locals {
  ou          = "dev"
  environment = "pkimbrel-dev"

  account = data.aws_caller_identity.current.account_id

  vpc_id    = data.aws_vpc.vpc_region1.id
  subnet_id = data.aws_subnets.subnets_region1.ids[0]

  domain     = "dev.paulkimbrel.com"
  app_domain = "api"


  default_tags = {
    ou          = local.ou
    environment = local.environment
  }
}

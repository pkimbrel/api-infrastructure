output "lambda" {
  value = var.file != "" ? aws_lambda_function.lambda_given[0] : aws_lambda_function.lambda_dummy[0]
}

output "execution_role" {
  value = aws_iam_role.lambda_role
}

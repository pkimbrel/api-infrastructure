resource "aws_lambda_function" "lambda_dummy" {
  count = var.file == "" ? 1 : 0

  function_name = local.full_name
  filename      = "${path.module}/dummy.zip"
  role          = aws_iam_role.lambda_role.arn
  runtime       = var.runtime
  handler       = var.handler
  timeout       = var.timeout
  memory_size   = var.memory_size
  layers        = var.layers
  tags          = var.tags

  environment {
    variables = var.environment
  }

  lifecycle {
    ignore_changes = [filename]
  }

  depends_on = [
    aws_iam_role.lambda_role,
    aws_cloudwatch_log_group.log_group,
  ]
}

resource "aws_lambda_function" "lambda_given" {
  count = var.file != "" ? 1 : 0

  function_name    = local.full_name
  source_code_hash = filebase64sha256(var.file)
  filename         = var.file
  role             = aws_iam_role.lambda_role.arn
  runtime          = var.runtime
  handler          = var.handler
  timeout          = var.timeout
  memory_size      = var.memory_size
  layers           = var.layers
  tags             = var.tags

  environment {
    variables = var.environment
  }

  depends_on = [
    aws_iam_role.lambda_role,
    aws_cloudwatch_log_group.log_group,
  ]
}

resource "aws_cloudwatch_log_group" "log_group" {
  name              = "/aws/lambda/${local.full_name}"
  retention_in_days = var.log_retention
}

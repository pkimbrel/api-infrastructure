variable "suite" {
  type        = string
  description = "Suite the lambda belongs to"
}

variable "name" {
  type        = string
  description = "Lambda name"
}

variable "runtime" {
  type        = string
  description = "(Optional) Lambda runtime"
  default     = "nodejs16.x"
}

variable "handler" {
  type        = string
  description = "(Optional) Lambda entry point"
  default     = "index.handler"
}

variable "timeout" {
  type        = number
  description = "(Optional) Timeout (in seconds)"
  default     = 30
}

variable "memory_size" {
  type        = number
  description = "(Optional) Memory Size"
  default     = 1024
}

variable "layers" {
  type        = list(any)
  description = "(Optional) Associated Layers"
  default     = []
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Tags"
  default     = {}
}

variable "environment" {
  type        = map(any)
  description = "(Optional) Environment variables"
  default     = {}
}

variable "log_retention" {
  type        = number
  description = "(Optional)Time to live for logs (in days)"
  default     = 30
}

variable "policies" {
  type        = map(any)
  description = "(Optional) Additional policies to attach to the lambda role"
  default     = {}
}

variable "file" {
  type        = string
  description = "(Optional) Lambda code to deploy (disables lifecycle rule)"
  default     = ""
}

data "aws_region" "current" {}

locals {
  full_name = "${var.suite}-${var.name}"
  region    = data.aws_region.current.name
}

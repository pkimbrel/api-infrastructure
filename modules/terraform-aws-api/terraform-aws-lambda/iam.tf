resource "aws_iam_role" "lambda_role" {
  name               = "lambda-${local.full_name}-${local.region}"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json

  inline_policy {
    name   = "lambda-${local.full_name}-logs-${local.region}"
    policy = data.aws_iam_policy_document.lambda_logs_permissions.json
  }

  dynamic "inline_policy" {
    for_each = var.policies
    content {
      name   = "lambda-${local.full_name}-${inline_policy.key}-${local.region}"
      policy = inline_policy.value
    }
  }
}

/**
 * Assume Role
 */
data "aws_iam_policy_document" "lambda_assume_role" {
  statement {

    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

/**
 *  Cloud Watch Logs
 */
data "aws_iam_policy_document" "lambda_logs_permissions" {
  statement {
    actions = [
      "logs:CreateLogGroup"
    ]
    effect    = "Allow"
    resources = [aws_cloudwatch_log_group.log_group.arn]
  }

  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    effect    = "Allow"
    resources = ["${aws_cloudwatch_log_group.log_group.arn}:*"]
  }
}

/**
 * CloudWatch Insights
 */

resource "aws_iam_role_policy_attachment" "lambda_insights" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLambdaInsightsExecutionRolePolicy"
}


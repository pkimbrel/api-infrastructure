resource "aws_apigatewayv2_api" "api" {
  name          = var.name
  description   = var.description
  protocol_type = "HTTP"

  cors_configuration {
    allow_headers     = setunion(["content-type"], var.allow_headers, [])
    allow_methods     = setunion(["GET", "OPTIONS"], var.allow_methods)
    allow_origins     = var.allow_origins
    expose_headers    = var.expose_headers
    allow_credentials = var.allow_credentials
    max_age           = var.max_age
  }
}

resource "aws_cloudwatch_log_group" "api_log_group" {
  name              = "/aws/api/${var.name}"
  retention_in_days = 30
}

resource "aws_apigatewayv2_authorizer" "authorizer" {
  count = (var.authorizer_issuer != null && var.authorizer_audience != null) ? 1 : 0

  api_id           = aws_apigatewayv2_api.api.id
  authorizer_type  = "JWT"
  identity_sources = ["$request.header.Authorization"]
  name             = "${var.name}-authorizer"

  jwt_configuration {
    audience = [var.authorizer_audience]
    issuer   = var.authorizer_issuer
  }
}

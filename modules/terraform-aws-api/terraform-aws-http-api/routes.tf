resource "aws_apigatewayv2_route" "route" {
  for_each = var.endpoints

  api_id               = aws_apigatewayv2_api.api.id
  route_key            = "${each.value.method} ${each.value.route}"
  authorization_type   = each.value.authorization_type
  authorizer_id        = (each.value.authorization_type == "JWT" && length(aws_apigatewayv2_authorizer.authorizer) > 0) ? aws_apigatewayv2_authorizer.authorizer.0.id : ""
  authorization_scopes = each.value.authorization_scopes
  target               = "integrations/${aws_apigatewayv2_integration.integration[each.key].id}"
}

resource "aws_apigatewayv2_integration" "integration" {
  for_each = var.endpoints

  api_id           = aws_apigatewayv2_api.api.id
  integration_type = "AWS_PROXY"

  connection_type      = "INTERNET"
  integration_method   = "POST"
  description          = "Integration for ${each.value.lambda} lambda"
  integration_uri      = "arn:aws:lambda:${var.region}:${var.account}:function:${each.value.lambda}"
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_lambda_permission" "permission" {
  for_each = var.endpoints

  action        = "lambda:InvokeFunction"
  function_name = each.value.lambda
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.api.execution_arn}/*/*${each.value.route}"
}


output "target_domain_name" {
  value       = aws_apigatewayv2_domain_name.custom_domain.domain_name_configuration[0].target_domain_name
  description = "The cloud front domain the API gateway resides in"
}

output "hosted_zone_id" {
  value       = aws_apigatewayv2_domain_name.custom_domain.domain_name_configuration[0].hosted_zone_id
  description = "The hosted zone the cloud front domain is hosted in"
}

output "api" {
  value       = aws_apigatewayv2_api.api
  description = "The api resource"
}

variable "name" {
  type        = string
  description = "Name of the API"
}

variable "description" {
  type        = string
  description = "Description of the API"
}

variable "domain" {
  type        = string
  description = "The domain for the API"
}

variable "subdomain" {
  type        = string
  description = "The subdomain of the API against the current environment"
}

variable "environment" {
  type        = string
  description = "The current environment"
}

variable "account" {
  type        = string
  description = "The account the API resides in"
}

variable "region" {
  type        = string
  description = "The region the API resides in"
}

variable "endpoints" {
  type        = map(any)
  description = "The endpoints for this API"
}

variable "allow_headers" {
  type        = list(any)
  description = "List of allowed headers (CORS)"
  default     = []
}

variable "allow_methods" {
  type        = list(any)
  description = "List of allowed methods (CORS)"
  default     = []
}

variable "allow_origins" {
  type        = list(any)
  description = "List of allowed origins (CORS)"
  default     = []
}

variable "expose_headers" {
  type        = list(any)
  description = "List of exposed headers (CORS)"
  default     = []
}

variable "allow_credentials" {
  type        = bool
  description = "Credential/Cookies allowed (CORS)"
  default     = false
}

variable "authorizer_issuer" {
  type        = string
  description = "Authorization Issuer Endpoint"
  default     = null
}

variable "authorizer_audience" {
  type        = string
  description = "Authorization Audience (Client ID)"
  default     = null
}

variable "max_age" {
  type        = number
  description = "Preflight cache time in seconds (CORS)"
  default     = 0
}

locals {
  fqdn = "${var.subdomain}.${var.domain}"
}

module "api_region1" {
  source = "./terraform-aws-http-api"

  name        = "${var.suite}-gateway"
  description = var.description

  domain      = var.domain
  subdomain   = var.subdomain
  environment = var.environment

  account = var.account
  region  = "us-east-1"

  authorizer_issuer   = var.authorizer_issuer
  authorizer_audience = var.authorizer_audience

  allow_headers     = var.allow_headers
  allow_methods     = var.allow_methods
  allow_origins     = var.allow_origins
  expose_headers    = var.expose_headers
  allow_credentials = var.allow_credentials

  endpoints = {
    for k, v in var.routes : k => {
      lambda               = "${var.suite}-${v.lambda}"
      route                = v.route
      method               = v.method
      authorization_type   = v.authorization_type
      authorization_scopes = v.authorization_scopes
    }
  }

  depends_on = [
    module.lambda_region1
  ]
}

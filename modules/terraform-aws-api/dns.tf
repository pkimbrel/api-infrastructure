data "aws_route53_zone" "zone" {
  name         = "${var.domain}."
  private_zone = false
}

resource "aws_route53_record" "dns_region1" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${var.subdomain}.${var.domain}"
  type    = "A"

  latency_routing_policy {
    region = "us-east-1"
  }

  set_identifier = "us-east-1"

  alias {
    name                   = module.api_region1.target_domain_name
    zone_id                = module.api_region1.hosted_zone_id
    evaluate_target_health = false
  }
}

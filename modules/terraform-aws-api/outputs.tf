output "api" {
  value = module.api_region1.api
}

output "lambda" {
  value = module.lambda_region1
}

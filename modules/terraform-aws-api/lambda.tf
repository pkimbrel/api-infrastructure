module "lambda_region1" {
  for_each = var.lambdas

  source = "./terraform-aws-lambda"

  suite = var.suite
  name  = each.key

  environment = merge({
    OU          = var.ou
    ENVIRONMENT = var.environment
  }, lookup(each.value, "environment", {}))

  runtime = each.value.runtime

  policies = each.value.policies
}

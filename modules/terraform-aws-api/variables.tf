variable "ou" {
  description = "Organizational unit for deployment"
  type        = string
  default     = "test"
}

variable "environment" {
  description = "Environment for deployment"
  type        = string
  default     = "sandbox"
}

variable "account" {
  description = "AWS Account"
  type        = string
}

variable "description" {
  description = "Description of the API"
  type        = string
}

variable "suite" {
  description = "Product Suite"
  type        = string
  default     = ""
}

variable "domain" {
  description = "Domain of the API (optional)"
  type        = string
  default     = ""
}

variable "subdomain" {
  description = "Subdomain of the API"
  type        = string
}

variable "authorizer_issuer" {
  description = "Authorizer issuance endpoint"
  type        = string
  default     = null
}

variable "authorizer_audience" {
  description = "Authorizer audience/client ID"
  type        = string
  default     = null
}

variable "lambdas" {
  description = "Map of lambdas for the API"
  type        = map(any)
}

variable "routes" {
  description = "List of API routes"
  type        = map(any)
}

variable "allow_headers" {
  type        = list(any)
  description = "List of allowed headers (CORS)"
  default     = []
}

variable "allow_methods" {
  type        = list(any)
  description = "List of allowed methods (CORS)"
  default     = []
}

variable "allow_origins" {
  type        = list(any)
  description = "List of allowed origins (CORS)"
  default     = []
}

variable "expose_headers" {
  type        = list(any)
  description = "List of exposed headers (CORS)"
  default     = []
}

variable "allow_credentials" {
  type        = bool
  description = "Credential/Cookies allowed (CORS)"
  default     = false
}

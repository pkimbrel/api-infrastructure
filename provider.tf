provider "aws" {
  region = "us-east-1"

  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
  profile    = "personal-dev"

  default_tags {
    tags = local.default_tags
  }
}
